﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BinaryTree.Builder;
using BinaryTree.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BinaryTree.Tests
{
    [TestClass]
    public class TreeBuilderTests
    {
        public const string ValidTree = "I, Study, English \r\n Study, Long, Time";
        public static TreeBuilder TreeBuilder { get; set; }
        public static NodeAnalyzer TestsAnalyzer { get; set; }

        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            TestsAnalyzer = new NodeAnalyzer();
        }

        [TestMethod]
        public void BuilderTree_CanBuildTreeWithRelationship()
        {
            var builder = new TreeBuilder();
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(ValidTree);
            writer.Flush();
            stream.Position = 0;

            var reader = new StreamReader(stream);
            Tree tree = builder.BuildTree(reader);

            Assert.IsNotNull(tree);
        }

        [TestMethod]
        public void BuildNode_CheckReturnNullNode()
        {
            var nodeValue = "#";
            Node node = TestsAnalyzer.BuildNode(new Dictionary<string, Node>(), nodeValue);
            Assert.IsNull(node);
        }

        [TestMethod]
        public void BuildNode_CheckReturnNode()
        {
            var nodeValue = "Test";
            var node = TestsAnalyzer.BuildNode(new Dictionary<string, Node>(), nodeValue);
            Assert.IsNotNull(node);
        }

        #region Exceptions
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void BuildTree_StreamReaderIsEmpty_ThrowsException()
        {
            TreeBuilder.BuildTree(null);
        }

        [TestMethod]
        [ExpectedException(typeof(TreeBuilderException), "Invalid number of words per line.")]
        public void CheckNodeValues_CountNodeValuesLessThenThree_ThrownException()
        {
            string line = "I, Study, Time, English";
            List<string> nodeValuesList = line.Split(',').Select(word => word.Trim()).ToList();
            TestsAnalyzer.CheckNodeValues(nodeValuesList);
        }

        [TestMethod]
        [ExpectedException(typeof(TreeBuilderException), "The node value is empty.")]
        public void CheckNodeValues_NodeValuesListIsEmpty_ThrownException()
        {
            List<string> nodeValues = new List<string>();
            TestsAnalyzer.CheckNodeValues(nodeValues);
        }
        #endregion

    }
}