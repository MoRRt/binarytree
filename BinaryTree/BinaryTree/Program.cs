﻿using System;
using System.IO;
using BinaryTree.Builder;
using BinaryTree.Models;

namespace BinaryTree
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TreeBuilder builder = new TreeBuilder();
                Tree tree = null;

                using (var reader = new StreamReader(@"..\..\test.txt"))
                {
                    try
                    {
                        tree = builder.BuildTree(reader);
                    }
                    catch (TreeBuilderException treeException)
                    {
                        Console.WriteLine("Error: " + treeException.Message);
                    }
                }
                if (tree != null)
                    Console.WriteLine("Building a tree is successful.");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: "+ e.Message);
            }
        }
    }
}
