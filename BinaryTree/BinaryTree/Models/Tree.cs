﻿namespace BinaryTree.Models
{
    public class Tree
    {
        public Node Root { get; set; }

        public Tree(Node root)
        {
            Root = root;
        }
    }
}