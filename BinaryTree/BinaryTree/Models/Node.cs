﻿namespace BinaryTree.Models
{
    public class Node
    {
        public string Value { get; set; }
        public Node Left { get; set; }
        public Node Right { get; set; }

        public Node(string value, Node left, Node right)
        {
            Left = left;
            Right = right;
            Value = value;
        }
    }
}