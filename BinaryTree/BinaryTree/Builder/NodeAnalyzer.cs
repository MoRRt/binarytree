﻿using System.Collections.Generic;
using System.Linq;
using BinaryTree.Models;

namespace BinaryTree.Builder
{
    public class NodeAnalyzer
    {
        /// <summary>
        /// If the values of the nodes are not valid, exceptions will be thrown
        /// </summary>
        /// <param name="nodeValues">List of nodes values</param>
        public void CheckNodeValues(List<string> nodeValues)
        {
            if (nodeValues.Count != 3)
                throw new TreeBuilderException("Invalid number of words per line.");
            
            if (nodeValues.Any(string.IsNullOrWhiteSpace))
                throw new TreeBuilderException("The node value is empty.");
        }

        /// <summary>
        /// Method for validate tree
        /// </summary>
        /// <param name="rootNodesList">List of root nodes</param>
        /// <param name="nodesCount"> Count of nodes at the graph</param>
        public void CheckTree(List<Node> rootNodesList, int nodesCount)
        {
            if (rootNodesList.Count == 0)
                throw new TreeBuilderException("The list of nodes is empty.");
            
            if (rootNodesList.Count > 1)
                throw new TreeBuilderException("The graph has several parts.");
          
            Queue<Node> queue = new Queue<Node>();
            Node rootNode = rootNodesList.First();
            queue.Enqueue(rootNode);
           
            int count = 0;
            HashSet<Node> traversedNodes = new HashSet<Node>();
            while (queue.Any())
            {
                Node node = queue.Dequeue();
                if (traversedNodes.Contains(node))
                    throw new TreeBuilderException("The graph has loops.");
                
                traversedNodes.Add(node);
                count++;
                if (node.Left != null)
                    queue.Enqueue(node.Left);
                
                if (node.Right != null)
                    queue.Enqueue(node.Right);
            }
            if (count != nodesCount)
                throw new TreeBuilderException("The graph has several parts.");
        }

        /// <summary>
        /// Get or build node. 
        /// If node has value "#" it means branch has been completed and has no children and method will return empty node.
        /// </summary>
        /// <param name="nodesDictionary"></param>
        /// <param name="nodeValue"></param>
        /// <returns></returns>
        public Node BuildNode(Dictionary<string, Node> nodesDictionary, string nodeValue)
        {
            if (nodeValue == "#")            
                return null;

            if (!nodesDictionary.ContainsKey(nodeValue))
            {
                Node node = new Node(nodeValue, null, null);
                nodesDictionary.Add(nodeValue, node);
            }
            return nodesDictionary[nodeValue];
        }
    }
}