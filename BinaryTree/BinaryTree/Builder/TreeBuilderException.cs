﻿using System;

namespace BinaryTree.Builder
{
    public class TreeBuilderException : Exception
    {
        public TreeBuilderException(string message) : base(message) { }
    }
}