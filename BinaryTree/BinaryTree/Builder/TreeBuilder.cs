﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BinaryTree.Models;

namespace BinaryTree.Builder
{
    public class TreeBuilder
    {
        private NodeAnalyzer NodeAnalyzer { get; }

        public TreeBuilder()
        {
            NodeAnalyzer = new NodeAnalyzer();
        }

        public Tree BuildTree(StreamReader streamReader)
        {
            if (streamReader == null)
                throw new NullReferenceException("Stream reader hasn't been found.");

            HashSet<Node> children = new HashSet<Node>();
            Dictionary<string, Node> nodesDict = new Dictionary<string, Node>();
            var line = streamReader.ReadLine();
            while (line != null)
            {
                List<string> nodeValuesList = line.Split(',').Select(word => word.Trim()).ToList();

                NodeAnalyzer.CheckNodeValues(nodeValuesList);

                Node rootNode = NodeAnalyzer.BuildNode(nodesDict, nodeValuesList[0]);
                rootNode.Left = NodeAnalyzer.BuildNode(nodesDict, nodeValuesList[1]);
                rootNode.Right = NodeAnalyzer.BuildNode(nodesDict, nodeValuesList[2]);

                if (!children.Contains(rootNode.Left))
                {
                    children.Add(rootNode.Left);
                }

                if (!children.Contains(rootNode.Right))
                {
                    children.Add(rootNode.Right);
                }

                line = streamReader.ReadLine();
            }

            List<Node> rootNodesList = nodesDict.Values.Except(children).ToList();
            NodeAnalyzer.CheckTree(rootNodesList, nodesDict.Count);
            Tree tree = new Tree(rootNodesList.First());
            return tree;
        }
    }
}